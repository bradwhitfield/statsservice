
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Brad Whitfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package mappings;

import java.util.ArrayList;
import java.util.UUID;

/**
 * A class that represents running stats submitted to the server. This is only
 * used when receiving stats to see if data is in a compatible format.
 *
 * For the 355 Individual Project, this is what allows for type checking.
 *
 * For the 471 Final Project, this class represents the stats collected for the IRB stuff.
 *
 * @author Brad Whitfield
 */
public class RunningStats {
    private UUID uuid;
    private boolean runMode;
    private ArrayList<Long> pace;

    public RunningStats(UUID uuid, boolean runMode, ArrayList<Long> pace) {
        this.uuid = uuid;
        this.runMode = runMode;
        this.pace = pace;
    }
}
