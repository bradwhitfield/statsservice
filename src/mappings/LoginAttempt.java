/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Brad Whitfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package mappings;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Takes a username and password in plain text and returns creates an object
 * that contains the username and a hashed password to try and authenticate with.
 *
 * This object will get passed to the LoginController to see if the user entered proper
 * credentials, and generate all that good login stuff.
 *
 * @author Brad Whtifield
 */
public class LoginAttempt {
    private String username;
    private String passwordHash;

    public LoginAttempt(String username, String password) {
        this.username = username;

        //TODO: Salt this
        //Generate a hash of the password
        try {
            //Hash with SHA 512
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
            byte output[] = messageDigest.digest(password.getBytes());

            //Store the value as BASE64 to save from having to parse to store in the DB
            this.passwordHash = new String(Base64.getEncoder().encode(output));
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            this.passwordHash = "";
        }
    }

    /**
     * Gets the plain text username for this attempt
     *
     * @return  The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Gets the hashed password for the user
     *
     * @return  The password as a Base64 hash
     */
    public String getPasswordHash() {
        return passwordHash;
    }
}
