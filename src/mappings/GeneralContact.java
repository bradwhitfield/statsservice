
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Brad Whitfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package mappings;

/**
 * A class that represents general contact request submitted to the server.
 * This is only used when a user tries to connect us through the site to check
 * if data is in a compatible format.
 *
 * For the 355 Individual Project, this is what allows for type checking.
 *
 * @author Brad Whitfield
 */
public class GeneralContact {
    private String name;
    private String contact;
    private String message;

    public GeneralContact(String name, String contact, String message) {
        this.name = name;
        this.contact = contact;
        this.message = message;
    }

    public GeneralContact(String name, String message) {
        this.name = name;
        this.message = message;
    }

    @Override
    public String toString() {
        return "GeneralContact{" +
                "name='" + name + '\'' +
                ", contact='" + contact + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
