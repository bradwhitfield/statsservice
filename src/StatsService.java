/**
 * Created by brad on 1/14/15.
 *
 * A service used for gathering our anonymous usage statistics for our Senior Project research.
 * This server application servers two functions, store JSON data of runs stats in MongoDB, and
 * return all stats for later use in developing our stats.
 */
public class StatsService {

    public static void main(String[] args) {
        //Start services (Mongo and Spark)
        new SparkService();
    }
}
