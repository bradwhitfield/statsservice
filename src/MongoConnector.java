import com.mongodb.*;
import com.mongodb.util.JSON;
import org.bson.types.ObjectId;

import java.net.UnknownHostException;

/**
 * The simple connector that connects to our DB to save and get stats and contact request.
 *
 * @author  Brad Whitfield
 */
public class MongoConnector {
    private static DB db;
    private static DBCollection statsCollection;
    private static DBCollection contactCollection;
    private static DBCollection issueCollection;
    private static DBCollection archiveCollection;

    /**
     * Creates the connection to the database and initializes the collections.
     *
     */
    public MongoConnector() {
        try {
            //Create connection to MongoDB running on localhost and
            //connect to the Running Stats database
            db = new MongoClient("localhost", 27017).getDB("RunStats");

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        statsCollection = db.getCollection("Stats");
        contactCollection = db.getCollection("Contact");
        issueCollection = db.getCollection("Issue");
        archiveCollection = db.getCollection("archive");
    }

    /**
     * Saves the contact request to the contact collection. The JSON is checked
     * for formatting.
     *
     * @param SavedContact  A string that represents the JSON of the contact information.
     */
    public void saveContact(String SavedContact) {
        DBObject dbObject = (DBObject) JSON.parse(SavedContact);
        contactCollection.insert(dbObject);
    }

    /**
     * Saves the bug report to the issue collection. The JSON is checked for formatting.
     *
     * @param SavedIssue    A string that represents the JSON of the bug report.
     */
    public void saveIssue(String SavedIssue) {
        DBObject dbObject = (DBObject) JSON.parse(SavedIssue);
        issueCollection.insert(dbObject);
    }

    /**
     * Points to the top of the stats collection so that the overall stats for the
     * project can be determined.
     *
     * @return  A cursor to the top of the stats collection.
     */
    public DBCursor getAllStats() {
        return MongoConnector.statsCollection.find();
    }

    /**
     * Gets all stats that meet the criteria of the project (more than 5 minutes).
     *
     * @return  A cursor to the top of the stats collection.
     */
    public DBCursor getAllGoodStats() {
        return MongoConnector.statsCollection.find(new BasicDBObject("paceSamples.4", new BasicDBObject("$exists", true)));
    }

    /**
     * Returns the most recent contact submitted to the server.
     *
     * @return  A DBCursor containing the most recent contact submission.
     */
    public DBCursor getMostRecentContact() {
        return MongoConnector.contactCollection.find().sort(new BasicDBObject("_id", -1)).limit(1);
    }

    /**
     * Returns the most recent issue submitted to the server.
     *
     * @return  A DBCursor containing the most recent issue submission.
     */
    public DBCursor getMostRecentIssue() {
        return MongoConnector.issueCollection.find().sort(new BasicDBObject("_id", -1)).limit(1);
    }

    /**
     * Returns 5 contact submissions to view.
     *
     * @param startIndex    The index to start at when returning contact submissions.
     * @return  The 5 contact request in JSON form, separated by new lines.
     */
    public String getFiveContacRequest(int startIndex) {
        DBCursor cursor = MongoConnector.contactCollection.find().skip(startIndex).limit(5);

        //I know this is a bad idea, but all well
        StringBuilder sb = new StringBuilder();
        while (cursor.hasNext()) {
            sb.append(cursor.next());
            if (cursor.hasNext()) {
                sb.append("\n");
            }
        }

        return sb.toString();
    }

    /**
     * Returns 5 issue submissions to view.
     *
     * @param startIndex    The index to start at when returning issue submissions.
     * @return  The 5 issue request in JSON form, separated by new lines.
     */
    public String getFiveIssues(int startIndex) {
        DBCursor cursor = MongoConnector.issueCollection.find().skip(startIndex).limit(5);

        StringBuilder sb = new StringBuilder();
        while (cursor.hasNext()) {
            sb.append(cursor.next());
            if (cursor.hasNext()) {
                sb.append("\n");
            }
        }

        return sb.toString();
    }

    /**
     * Deletes a contact submission from the collection that matches the passed id.
     *
     * @param contactID The id of the document to delete.
     */
    public void deleteContact(String contactID) {
        //Create the Object ID to use to identify the document in MongoDB
        ObjectId id = new ObjectId(contactID);
        BasicDBObject queryObject = new BasicDBObject();
        queryObject.append("_id", id);

        //Run the query to delete the document.
        MongoConnector.contactCollection.remove(queryObject);
    }

    /**
     * Deletes an issue submission from the collection that matches the passed id.
     *
     * @param contactID The id of the document to delete.
     */
    public void deleteIssue(String contactID) {
        //Create the Object ID to use to identify the document in MongoDB
        ObjectId id = new ObjectId(contactID);
        BasicDBObject queryObject = new BasicDBObject();
        queryObject.append("_id", id);

        //Run the query to delete the document.
        MongoConnector.issueCollection.remove(queryObject);
    }

    /**
     * Moves a contact submission from the contact collection to the archive collection.
     *
     * @param contactID The id of the document to archive.
     */
    public void archiveContact(String contactID) {
        //Create the Object ID to use to identify the document in MongoDB
        ObjectId id = new ObjectId(contactID);
        BasicDBObject queryObject = new BasicDBObject();
        queryObject.append("_id", id);

        BasicDBObject excludeID = new BasicDBObject();
        excludeID.append("_id", 0);

        DBCursor cursor = MongoConnector.contactCollection.find(queryObject, excludeID);

        if (cursor.hasNext()) {
            //Add the selected document to the archive
            DBObject dbObject = (DBObject) JSON.parse(cursor.next().toString());
            MongoConnector.archiveCollection.insert(dbObject);

            //Remove it from the main collection.
            MongoConnector.contactCollection.remove(queryObject);
        }
    }

    /**
     * Moves a contact submission from the contact collection to the archive collection.
     *
     * @param contactID The id of the document to archive.
     */
    public void archiveIssue(String contactID) {
        //Create the Object ID to use to identify the document in MongoDB
        ObjectId id = new ObjectId(contactID);
        BasicDBObject queryObject = new BasicDBObject();
        queryObject.append("_id", id);

        BasicDBObject excludeID = new BasicDBObject();
        excludeID.append("_id", 0);

        DBCursor cursor = MongoConnector.issueCollection.find(queryObject, excludeID);

        if (cursor.hasNext()) {
            //Add the selected document to the archive
            DBObject dbObject = (DBObject) JSON.parse(cursor.next().toString());
            MongoConnector.archiveCollection.insert(dbObject);

            //Remove it from the main collection.
            MongoConnector.issueCollection.remove(queryObject);
        }
    }

    /**
     * Gets a JSON document containing that counts for the three main collections, issue, contact, and stats.
     *
     * @return A JSON document containing the total count for each collection ({"issue": 0, "comments": 0, "runs": 0}).
     */
    public String getCountsForDashboard() {
        return "{\"issues\": " + MongoConnector.issueCollection.count() +
                ", \"comments\": " + MongoConnector.contactCollection.count() +
                ", \"runs\": " + MongoConnector.statsCollection.count() + "}";
    }
}