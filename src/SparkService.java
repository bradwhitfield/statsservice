
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Brad Whitfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import Authentication.LoginController;
import Authentication.PostgresConnector;
import com.google.gson.JsonSyntaxException;
import com.google.gson.Gson;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.util.JSON;
import mappings.GeneralContact;
import mappings.LoginAttempt;
import mappings.ProblemContact;
import spark.servlet.SparkApplication;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import static spark.Spark.*;
import static spark.SparkBase.externalStaticFileLocation;

/**
 * A web service based on the Spark micro-framework. The service acts as a
 * very simple ReST API and serves up the static files for the Cadence Control
 * web page. The ReST API is used for storing contact request and bug reports
 * that are sent from either the web site or Android app, and running stats sent
 * from the Android device.
 *
 * @author  Brad Whitfield
 */
public class SparkService implements SparkApplication {
    private static MongoConnector mongoConnector;

    //localhost is the only one allowed to view the run stats, at the moment
    private static final String allowed = "0:0:0:0:0:0:0:1";
    //CC page location
    private static final String pathToCCPage = "/var/www/cc/public_html";

    public SparkService() {
        this.init();
    }

    /**
     * Starts the service to serve web pages and act as the ReST API. The API
     * only excepts and gives data in a JSON format.
     *
     * The API responds to the following commands
     * <ul>
     *     <li>POST: /run-stats - Stores a users stats for an individual run.</li>
     *     <li>POST: /contact - Stores a submission to contact us.</li>
     *     <li>POST: /problem - Stores bug reports or issues submitted on the website.</li>
     *     <li>GET: /all-stats - Only works from authorized IP (currently none) and logged in user.</li>
     * </ul>
     */
    public void init() {
        //Create the connection to MongoDB for spark to talk to
        mongoConnector = new MongoConnector();

        //Create the connection to PostgreSQL which only contains user data
        try {
            new PostgresConnector();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        //Specifies where on the server the files should be
        externalStaticFileLocation(pathToCCPage);

        /**
         * Stores the general comments and questions that are submitted by the user.
         *
         * Input needs to look like the following to return a 201.
         *
         * {"name": "name", "contact" "email or phone": "message": "message body"}
         */
        post("/contact", (request, response) -> {
            System.out.println(request.body());

            try {
                //Check the incoming data for proper format
                GeneralContact contact = new Gson().fromJson(request.body(), GeneralContact.class);

                if (contact != null) {
                    //If good, save the contact form in mongo
                    mongoConnector.saveContact(request.body());

                    response.status(201);
                    return "Good\n";
                } else {
                    response.status(400);
                    return "Data sent is not in proper format.";
                }
            } catch (JsonSyntaxException e) {
                e.printStackTrace();

                //Inform the client something went wrong
                response.status(400);
                return "Something went wrong. A trained monkey is working on your problem.";
            }
        });

        /**
         * Stores the issues submitted by users, and the exceptions captured by the app.
         *
         * Data is not restricted to a specific format here since issues can look different.
         */
        post("/problem", (request, response) -> {

            try {
                //Check the incoming data for proper format
                ProblemContact problem = new Gson().fromJson(request.body(), ProblemContact.class);

                if (problem != null) {
                    //If good, save the contact form in mongo
                    mongoConnector.saveIssue(request.body());

                    response.status(201);
                    return "Good\n";
                } else {
                    response.status(400);
                    return "Data sent is not in proper format.";
                }
            } catch (JsonSyntaxException e) {
                e.printStackTrace();

                //Inform the client something went wrong
                response.status(400);
                return "Something went wrong. A trained monkey is working on your problem.";
            }
        });

        /**
         * Verifies the requesting IP is the is allowed to look at the run stats.
         */
        before("/stats", (request, response) -> {
            if (!request.ip().equals(allowed)) {
                halt(403, "Not allowed!");
            }
        });

        get("/stats", (request, response) -> {
            //Build up the response output
            StringBuilder output = new StringBuilder(300);

            DBCursor cursor = mongoConnector.getAllGoodStats();

            //Store each JSON document in the output as a string
            //The returned output will be parsed on the client side
            while (cursor.hasNext()) {
                output.append(cursor.next().toString());
                output.append(",");
            }
            output.deleteCharAt(output.length() - 1);

            response.status(200);

            //Send response either way.
            return output.toString();
        });

        /**
         * Verifies the requesting IP is the is allowed to look at the run stats.
         */
        before("/all-stats", (request, response) -> {
            if (!request.ip().equals(allowed)) {
                halt(403, "Not allowed!");
            }
        });

        /**
         * Returns every JSON document in the RunStats collection.
         */
        get("/all-stats", (request, response) -> {
            //Build up the response output
            StringBuilder output = new StringBuilder(300);

            //Only allow the currently allowed IP to get all stats
            System.out.println(request.ip());
            DBCursor cursor = mongoConnector.getAllStats();

            //Store each JSON document in the output as a string
            //The returned output will be parsed on the client side
            while (cursor.hasNext()) {
                output.append(cursor.next().toString());
                output.append(",");
            }

            response.status(200);

            //Send response either way.
            return output.toString();
        });

        /**
         * Verifies a user is logged in before allowing them to get the most recent information.
         */
        before("/recent/*", (request, response) -> {
            if (!LoginController.isLoggedIn(request)) {
                halt(401);
            }
        });

        /**
         * Gets the most recent contact or issue submission, depending on the second
         * parameter in the URI.
         */
        get("/recent/*", (request, response) -> {
            response.type("application/json");

            DBCursor cursor;
            if (request.splat()[0].equals("contact")) {
                cursor = mongoConnector.getMostRecentContact();
            } else if (request.splat()[0].equals("issue")) {
                cursor = mongoConnector.getMostRecentIssue();
            } else {
                //501: Not Implemented
                response.status(501);
                return "URI " + request.pathInfo() + " not understood.";
            }

            response.status(200);
            return cursor.next().toString();
        });

        /**
         * Verifies that a user is logged in before using any dashbaord API or web page
         */
        before("/dashboard/*", (request, response) -> {
            if (!LoginController.isLoggedIn(request)) {
                response.redirect("/login");
                halt(401, "Please <a href='../login/'>Login</a>!");
            }
        });

        /**
         * Gets the number of contact, issues, and runs currently in each collection.
         */
        get("/dashboard/counts", (request, response) -> {
            response.type("application/json");
            response.status(200);
            return mongoConnector.getCountsForDashboard();
        });

        /**
         * Gets the logged in user to fill in the navbar
         */
        get("/dashboard/loggedinuser", (request, response) -> {
            response.type("application/json");
            response.status(200);

            //Get the current logged in user and return it as JSON
            String username = LoginController.getLoggedInUser(request);
            return "{\"username\": \"" + username + "\"}";
        });

        /**
         * Gets five contact request from the contact collection starting at either 0, or whatever
         * index is passed to the server.
         *
         * To get a starting location other than 0, document must contain the "start" property.
         *
         * {"start": 10}
         */
        get("/dashboard/contact", (request, response) -> {
            response.type("text/json");

            //If there was a number passed.
            BasicDBObject json = null;
            try {
                json = (BasicDBObject) JSON.parse(java.net.URLDecoder.decode(request.queryString(), "UTF-8"));
                if (json.containsField("start")) {
                    response.status(200);
                    return mongoConnector.getFiveContacRequest(json.getInt("start"));
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            //If there was not a number passed
            return mongoConnector.getFiveContacRequest(0);
        });

        /**
         * Gets five issue submissions from the issue collection starting at either 0, or whatever
         * index is passed to the server.
         *
         * To get a starting location other than 0, document must contain the "start" property.
         *
         * {"start": 10}
         */
        get("/dashboard/issue", (request, response) -> {
            response.type("text/json");
            response.status(200);

            //If there was a number passed.
            BasicDBObject json = null;
            try {
                json = (BasicDBObject) JSON.parse(java.net.URLDecoder.decode(request.queryString(), "UTF-8"));
                if (json.containsField("start")) {
                    response.status(200);
                    return mongoConnector.getFiveIssues(json.getInt("start"));
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            //If there was not a number passed
            return mongoConnector.getFiveIssues(0);
        });

        /**
         * Deletes the specified issue if the object id exist.
         *
         * Document must contain "id", as shown below.
         *
         * {"id": "55347bd22736559617150b98"}
         */
        delete("/dashboard/issue", (request, response) -> {
            response.type("text/json");

            //Attempt to get the ID from the json document sent from the client
            BasicDBObject json = (BasicDBObject) JSON.parse(request.body());
            if (json.containsField("id")) {
                mongoConnector.deleteIssue(json.getString("id"));
                response.status(200);
                return "{\"status\": \"Good\"}";
            }

            response.status(404);
            return "{\"status\": \"Bad\"}";
        });

        /**
         * Deletes the specified contact if the object id exist.
         *
         * Document must contain "id", as shown below.
         *
         * {"id": "55347bd22736559617150b98"}
         */
        delete("/dashboard/contact", (request, response) -> {
            response.type("text/json");

            //Attempt to get the ID from the json document sent from the client
            BasicDBObject json = (BasicDBObject) JSON.parse(request.body());
            if (json.containsField("id")) {
                mongoConnector.deleteContact(json.getString("id"));
                response.status(200);
                return "{\"status\": \"Good\"}";
            }

            response.status(404);
            return "{\"status\": \"Bad\"}";
        });

        /**
         * Moves a contact to the archive collection if the object id exist.
         *
         * Document must contain "id", as shown below.
         *
         * {"id": "55347bd22736559617150b98"}
         */
        put("/dashboard/archive/contact", (request, response) -> {
            response.type("text/json");

            //Attempt to get the ID from the json document sent from the client
            BasicDBObject json = (BasicDBObject) JSON.parse(request.body());
            if (json.containsField("id")) {
                mongoConnector.archiveContact(json.getString("id"));
                response.status(200);
                return "{\"status\": \"Good\"}";
            }

            response.status(404);
            return "{\"status\": \"Bad\"}";
        });

        /**
         * Moves a issue to the archive collection if the object id exist.
         *
         * Document must contain "id", as shown below.
         *
         * {"id": "55347bd22736559617150b98"}
         */
        put("/dashboard/archive/issue", (request, response) -> {
            response.type("text/json");

            //Attempt to get the ID from the json document sent from the client
            BasicDBObject json = (BasicDBObject) JSON.parse(request.body());
            if (json.containsField("id")) {
                mongoConnector.archiveIssue(json.getString("id"));
                response.status(200);
                return "{\"status\": \"Good\"}";
            }

            response.status(404);
            return "{\"status\": \"Bad\"}";
        });

        /**
         * Checks if the user is already logged in and routes them back to the dashboard if they are.
         */
        before("/login/", (request, response) -> {
            if (LoginController.isLoggedIn(request)) {
                response.redirect("/dashboard/");
                halt();
            }
        });

        /**
         * Takes login credentials and creates a session for the user if they are correct.
         *
         * Documents must be in the form below.
         *
         * {"username": "username", "password": "password"}
         */
        post("/login", (request, response) -> {
            request.session(true);

            response.type("application/json");
            response.status(200);

            //I don't like the way GSON handles this, so I'm going to
            //create a json object from the body with Mongo's BasicDBObject
            //and pass that to the constructor of this class
            BasicDBObject json = (BasicDBObject) JSON.parse(request.body());
            LoginAttempt loginAttempt = new LoginAttempt(json.getString("username"), json.getString("password"));

            if (LoginController.isGoodCredentials(loginAttempt)) {
                //If the user logged in, setup a session with a new AuthToken
                LoginController.issueToken(request, loginAttempt);

                //Tell the user that the login was successful, and to go to the dashboard
                return "{\"status\": \"Good\", \"redirect\": \"/cc/dashboard/\"}";
            }

            return "{\"status\": \"Bad\", \"error\": \"Incorrect username or password.\"}";
        });

        /**
         * Deletes the session for the user and all auth tokens.
         */
        post("/logout", (request, response) -> {
            response.status(201);
            LoginController.logOut(request);

            return "{\"status\": \"Good\"}";
        });
    }
}
