/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Brad Whitfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var serverURL = "http://localhost:4567/";

function login() {
    var apiurl = serverURL + 'login';

    //Generic error handler since this is only used by me, really
    var errorCallback = function(data) {
        console.log("Error happened!");
        console.log(data);

        $("#error").html("An error occurred while attempting to log in. Please try again.");
    };

    var loginCallback = function(data) {
        if (data.status == "Good") {
            window.location.replace(data.redirect);
        }
        else {
            $("#error").html(data.error);
        }
    };

    var data = "{\"username\": \"" + $("#username").val() + "\", \"password\": \"" + $("#password").val() + "\"}";

    $.ajax({
        type: "POST",
        url: apiurl,
        data: data,
        dataType: 'json',
        json: 'callback'
    }).done(loginCallback).error(errorCallback);
}

$('#login-button').click(login());

$(document).keypress(function(event) {
    if (event.which == 13) {
        login();
    }
});