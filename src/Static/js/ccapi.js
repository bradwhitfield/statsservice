
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Brad Whitfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * The code that allows the web page to post new comments or contacts
 * to the server. This uses jQuery for most of the manipulation, so it
 * will need to be included before this file. The POST commands use jQuery's
 * .ajax command to send data to the server.
 */

var serverURL = "http://localhost:4567/";

$('#submitContact').click(function (event) {
    //Prevent the URL from appending a #
    event.preventDefault();

	var apiurl = serverURL + 'contact';
    //Get data to construct the JSON.
    var nameField = $('#name');
    var contactField = $('#contact-method-general');
    var messageField = $('#message');

    //Simple verification
    if (messageField.val() == "") {
      messageField.popover({title:'Please Fill In', content: 'Test'})
          .blur(function() {
            $(this).popover('hide');
          });
      messageField.popover('show');
      return;
    }

    //TODO: Make this a modal instead of a JS alert.
	var successCallback = function() {
		alert("Thank you for your submission!");
	};

    //Make the JSON document.
    var data = '{ name: "' + nameField.val() + '", contact: "' + contactField.val() +
            '", message: "' + messageField.val() + '"}';

    //call POST api to save contact request
	$.ajax({
		type: "POST",
		url: apiurl,
		data: data,
		success: successCallback
	});

    //Clear the fields
    nameField.val("");
    contactField.val("");
    messageField.val("");
});

$('#submitProblem').click(function (event) {
    //Prevent the URL from appending a #
    event.preventDefault();

    var apiurl = serverURL + 'problem';
    //Get data to construct the JSON.
    var deviceField = $('#device');
    var contactField = $('#contact-method-issue');
    var problemField = $('#problem');

    //TODO: Make this a modal instead of a JS alert.
    var successCallback = function() {
      alert("Thank you for your submission!");
    };

    //Simple verification
    var data = '{ device: "' + deviceField.val() + '", contact: "' + contactField.val() +
        '", problem: "' + problemField.val() + '"}';

    //call POST api to save contact request
    $.ajax({
      type: "POST",
      url: apiurl,
      data: data,
      success: successCallback
    });

    //Clear the fields
    deviceField.val("");
    contactField.val("");
    problemField.val("");
});