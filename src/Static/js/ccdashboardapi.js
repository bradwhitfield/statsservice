
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Brad Whitfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * When the main dashboard page loads, this jQuery is run to get the latest
 * general contact and issue submitted to the website, or through the app.
 */
$(document).ready(function() {
    var apiurl = generateApiUrl('recent/contact');

    //Generic error handler since this is only used by me, really
    var errorCallback = function(data) {
        console.log("Error happened!");
        console.log(data);
    };

    //The function to add the info to the recent-contact section
    var contactCallback = function(data) {
        $("#recent-contact").html(createComment(data));
    };

    //Setup the GET request for the most recent contact submission
    $.ajax({
        type: "GET",
        url: apiurl,
        dataType: 'json',
        json: 'callback'
    }).done(contactCallback).error(errorCallback);

    //Change the url to get the most recent issue now
    apiurl = generateApiUrl('recent/issue');

    //The function to add the info to the recent-issue section
    var issueCallback = function(data) {
        $("#recent-issue").html(createIssue(data));
    };

    //Setup the GET request for the most recent issue
    $.ajax({
        type: "GET",
        url: apiurl,
        dataType: 'json',
        json: 'callback'
    }).done(issueCallback).error(errorCallback);

    //Change the url to get the counts for each category the dashboard shows
    apiurl = generateApiUrl('dashboard/counts');

    //The function that adds the info to the dashboard views
    var countsCallback = function(data) {
        $("#comment-count").html(data.comments);
        $("#run-count").html(data.runs);
        $("#issue-count").html(data.issues);
    };

    //Setup the GET request for the counts in the dashboard
    $.ajax({
        type: "GET",
        url: apiurl,
        dataType: 'json',
        json: 'callback'
    }).done(countsCallback).error(errorCallback);

    //Sets the logged in user on the nav bar
    getLoggedInUser();
});