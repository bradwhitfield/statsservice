/*!
 * Start Bootstrap - Grayscale Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    // jQuery to handle contact page - display issue or general
    $('#issue').bind('click', function(event) {
        //Display issue form
        if ($("#issue-form").is(":visible")) {
            $("#issue-form").hide("slow");
        }
        else if ($("#general-form").is(":visible")) {
            $("#general-form").hide("slow");
            $("#issue-form").show("slow");
        }
        else {
            $("#issue-form").show("slow");
        }
        event.preventDefault();
        $("#issue-form").promise().done(function() {$("html, body").animate({ scrollTop: $(document).height()-$(window).height() });});
    });
    $('#general').bind('click', function(event) {
        //Display general contact page
        if ($("#issue-form").is(":visible")) {
            $("#issue-form").hide("slow");
            $("#general-form").show("slow");
        }
        else if ($("#general-form").is(":visible")) {
            $("#general-form").hide("slow");
        }
        else {
            $("#general-form").show("slow");
        }
        event.preventDefault();
        $("#general-form").promise().done(function() {$("html, body").animate({ scrollTop: $(document).height()-$(window).height() });});
    });
});

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});