/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Brad Whitfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//Generic error handler since this is only used by me, really
errorCallback = function(data) {
    console.log("Error happened!");
    console.log(data);
};

/**
 * Creates a simple html element for a comment posted by a user.
 *
 * @param json  The json from the server with the information about that contact submission.
 * @returns {string}    The HTML in string form to drop on the web page somewhere.
 */
function createComment(json) {
    return "<h4><strong>Name:</strong></h4><p>" + json.name + "</p><h4><strong>Email:</strong>" +
    "</h4><p>" + json.contact + "</p><h4><strong>Message:</strong></h4><p></p>" + json.message + "</p>";
}

/**
 * Creates a simple html element for an issue posted by a user or the app.
 *
 * @param json  The json from the server with the information about that issue submission.
 * @returns {string}    The HTML in string form to drop on the web page somewhere.
 */
function createIssue(json) {
    return "<h4><strong>Device:</strong></h4><p>" + json.device +"</p><h4><strong>Contact:" +
        "</strong></h4><p>" + json.contact + "</p><h4><strong>Problem:</strong></h4><p>" + json.problem +"</p>";
}

/**
 * Fills in the logged in user on the nav bar.
 */
function getLoggedInUser() {
    //Change the url to get the logged in username
    apiurl = generateApiUrl('dashboard/loggedinuser');

    //The function that sets the username in the nav bar
    var userCallback = function(data) {
        $("#logged-in-user").html(data.username);
    };

    //Setup the GET request to get the logged in user
    $.ajax({
        type: "GET",
        url: apiurl,
        dataType: 'json',
        json: 'callback'
    }).done(userCallback).error(errorCallback);
}

/**
 * Gets the URL for the api call. Since we are doing some things with proxying that make
 * this a bit difficult to get dynamically, this will be our single place to edit when
 * this is finally deployed to the server.
 *
 * @param uri   The URI to append to the current URL to get the address for the API .
 * @returns {string}    Returns the full URL for the API call.
 */
function generateApiUrl(uri) {
    return "http://localhost:4567/" + uri;
}