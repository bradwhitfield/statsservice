/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Brad Whitfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//Current count for issue submissions
issueIndex = 1;
totalPages = 0;

$(document).ready(function() {
    getLoggedInUser();
    getIssues(0);

    //Change the url to get the counts for each category the dashboard shows
    apiurl = generateApiUrl('dashboard/counts');

    //The function that adds the info to the dashboard views
    var countsCallback = function(data) {
        var html = "<li class='disabled' id='previous-page'><a id='previous-page-link' href='#'>«</a></li>"

        //Add pagination
        for (var i = 0; i < data.issues / 5; i++) {
            html += "<li class='page' id=" + (i+1) + "><a href='#'>" + (i+1) + "</a></li>";
            totalPages += 1;
        }

        if (totalPages == 1) {
            html += "<li class='disabled' id='next-page'><a id='next-page-link' href='#'>»</a></li>";
        }
        else {
            html += "<li id='next-page'><a id='next-page-link' href='#'>»</a></li>";
        }

        $("#page-selection").html(html);
        $("#1").addClass("active");
        $("#1").addClass("current-page");

        //Make the next page button work if it's not currently disabled
        $("#next-page-link").click(function(event) {
            event.preventDefault();

            if (!$("#next-page").hasClass("disabled")) {
                //Grab current page and generate the page
                getPage(parseInt($(".current-page")[0].id) + 1);
            }
        });

        //Make the previous page button work if it's not currently disabled
        $("#previous-page-link").click(function(event) {
            event.preventDefault();

            if (!$("#previous-page").hasClass("disabled")) {
                //Grab current page and generate the page
                getPage(parseInt($(".current-page")[0].id) - 1);
            }
        });

        //Handles figuring out what page was clicked, and generates that page
        $(".page").click(function(event) {
            event.preventDefault();
            getPage(event.currentTarget.id);
        })
    };

    //Setup the GET request for the counts in the dashboard
    $.ajax({
        type: "GET",
        url: apiurl,
        dataType: 'json',
        json: 'callback'
    }).done(countsCallback).error(errorCallback);
});

function getPage(pageNumber) {

    console.log(pageNumber);

    //The area containing the issue pages
    var issueSubmission = $("#issue-submissions");

    //Set the index appropriately
    issueIndex = ((pageNumber * 5) - 4);

    //Fade out the old stuff and delete it after the fade out is complete
    issueSubmission.fadeOut("fast", function() {
        issueSubmission.html("");

        //Fill in with new stuff
        //Each page has five, so start at the previous pages end
        getIssues((pageNumber * 5) - 5);

        //Fade in the new stuff and scroll to the top of the page when
        //the fade in is complete
        issueSubmission.fadeIn("fast", function() {
            $("html, body").animate({scrollTop: 0}, "fast");
        });

        $(".current-page").removeClass("active");
        $(".current-page").removeClass("current-page");
        $("#" + pageNumber).addClass("active");
        $("#" + pageNumber).addClass("current-page");

        //Configure back and forward to work properly.
        if (pageNumber > 1) {
            $("#previous-page").removeClass("disabled");
        }
        else {
            $("#previous-page").addClass("disabled");
        }

        if (pageNumber == totalPages) {
            $("#next-page").addClass("disabled");
        }
        else {
            $("#next-page").removeClass("disabled");
        }
    });
}

function getIssues(startIndex) {
    //Change the url to get the logged in username
    apiurl = generateApiUrl('dashboard/issue');

    //The function that sets up the views for the issues
    var issueCallback = function(data) {
        //The server sends back multiple JSON docs as text
        //Each doc is on a new line
        var docs = data.split("\n");

        var issueSubmission = $("#issue-submissions");

        for (var i in docs) {
            var d = JSON.parse(docs[i]);

            var html = "<div class='row' id='" + d._id.$oid + "'><div class='col-md-12'><div class='panel panel-red'><div class='panel-heading'>" +
                "<h3 class='panel-title'>Submission: " + issueIndex + "</h3></div><div class='panel-body'>" +
                createIssue(d) + "</div>" +
                "<button class='btn btn-danger' onclick='deleteDocument(\"" + d._id.$oid + "\")'>Delete</button> " +
                "<button class='btn btn-warning' onclick='archiveDocument(\"" + d._id.$oid + "\")'>Archive</button>" +
                "</div></div></div>";

            issueIndex += 1;

            issueSubmission.html(issueSubmission.html() + html);
        }
    };

    var data = "{\"start\": " + startIndex + "}";

    $.ajax({
        type: "GET",
        url: apiurl,
        dataType: 'text',
        data: data,
        text: 'callback'
    }).done(issueCallback).error(errorCallback);
}

function deleteDocument(id) {
    var apiurl = generateApiUrl("dashboard/issue");

    var data = "{\"id\": \"" + id + "\"}";

    var deleteCallback = function(data) {
        $(("#" + id)).fadeOut("slow");
    };

    $.ajax({
        type: "DELETE",
        url: apiurl,
        dataType: 'json',
        data: data,
        json: 'callback'
    }).done(deleteCallback).error(errorCallback);
}

function archiveDocument(id) {
    var apiurl = generateApiUrl("dashboard/archive/issue");

    var data = "{\"id\": \"" + id + "\"}";

    var deleteCallback = function(data) {
        $(("#" + id)).fadeOut("slow");
    };

    $.ajax({
        type: "PUT",
        url: apiurl,
        dataType: 'json',
        data: data,
        json: 'callback'
    }).done(deleteCallback).error(errorCallback);
}