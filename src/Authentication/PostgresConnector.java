package Authentication;

import java.sql.*;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * A singleton-ish connections to the PostgreSQL Database that stores the authentication and auth tokens.
 *
 * @author Brad Whitfield
 */
public class PostgresConnector {
    private static Connection connection;

    //TODO: Get db login from XML file.
    private static final String connectionString = "jdbc:postgresql://localhost:5432/webauth";

    private static final Calendar UTC = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

    /**
     * Attempts to create a connection to the PostgreSQL data webauth on localhost.
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public PostgresConnector() throws SQLException, ClassNotFoundException {
        //Attempt to connect to the database
        connection = DriverManager.getConnection(connectionString);
        connection.setAutoCommit(false);
    }

    /**
     * Takes in the authtoken code the user attempted to use and gets the details for that token.
     *
     * @param authToken The authtoken code the users session contains
     * @return  The complete authtoken that matches that code, if it exist, still.
     */
    public static AuthToken getAuthToken(String authToken) {
        String selection = "SELECT * FROM authtokens WHERE authToken='" + authToken + "';";
        AuthToken foundAuthToken;

        try {
            //Execute the query
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selection);

            //If there is an AuthToken found
            if (resultSet.next()) {
                //Create the AuthToken instance of the result
                foundAuthToken = new AuthToken(resultSet.getString("authtoken"), resultSet.getTimestamp("createddate"),
                        resultSet.getString("ip"), resultSet.getString("username"));
            }
            else {
                //No AuthToken found
                foundAuthToken = null;
            }

            //Close Connections
            resultSet.close();
            statement.close();

            return foundAuthToken;
        }
        catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Removes an expired or logged out authtoken from the database.
     *
     * @param username  The user who's authtokens are no longer valid.
     */
    public static void deleteAuthToken(String username) {
        String delete = "DELETE FROM authtokens WHERE username='" + username + "';";

        try {
            //Execute the query
            Statement statement = connection.createStatement();
            statement.executeUpdate(delete);

            //Close connection and force update in database
            statement.close();
            connection.commit();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves newly created authtokens to the database.
     *
     * @param authToken The new authtoken to be saved.
     * @return  If the authtoken was saved correctly.
     */
    public static boolean saveAuthToken(AuthToken authToken) {
        //The statement to prepare
        String saveAuthToken = "INSERT INTO authtokens (authtoken, createddate, username, ip) VALUES (?, ?, ?, ?);";

        try {
            //Prepare the statement
            PreparedStatement statement = connection.prepareStatement(saveAuthToken);
            statement.setString(1, authToken.getAuthToken());
            statement.setTimestamp(2, new Timestamp(authToken.getCreatedDate().getTime()));
            statement.setString(3, authToken.getUser());
            statement.setString(4, authToken.getIP());

            statement.execute();

            return true;
        }
        catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Checks if the user credentials are correct.
     *
     * @param username  The username
     * @param password  The password hash
     * @return  If they credentials were in the database.
     */
    public static boolean checkUser(String username, String password) {
        //Get the password hash
        String check = "SELECT password FROM users WHERE username='" + username + "';";
        boolean isCorrect = false; //Assume password is wrong till proven otherwise

        try {
            //Execute the query
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(check);

            //If username exist
            if (resultSet.next()) {
                //Check the passwords match
                if (password.equals(resultSet.getString("password"))) {
                    isCorrect = true;
                }
            }

            //Close connections
            statement.close();
            resultSet.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return isCorrect;
    }
}
