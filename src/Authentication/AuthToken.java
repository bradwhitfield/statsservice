package Authentication;

import spark.Request;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;

/**
 * <strong>DO NOT USE THIS LIBRARY</strong>
 * <p>A simple library that manages authentication tokens for a web application.
 * This library is not done by a security professional, so no one should use it.
 * The author created this for an application that doesn't have to be very
 * secure, and because he wanted to write one at least once.
 * </p>
 * <p>This library creates and manages authentication tokens for a simple web application.
 * The authentication tokens created using the SecureRandom library built into Java. The
 * random numbers generated are then digested in a SHA-512 hash and converted to BASE64
 * to be stored on the client machine.
 * </p>
 * <p>Default authentication tokens only last for 1 hour. Due to the limited nature of this
 * library, this can only be changed by altering the source file.
 * </p>
 * <p>The tokens are expired if the date on the server shows the token as being over an
 * hour old, or if the users IP address changes. If the token checks out as bad, this
 * library will delete it from the database.
 * </p>
 * <p>The database used to store the authentication tokens is a PostgreSQL 9.4 database.</p>
 *
 * @author  Brad Whitfield
 */
public class AuthToken {
    private String authToken;
    private Date createdDate;
    private String IP;
    private String user;

    //Number of hours for a token to live
    private static final int LIFESPAN = 1;

    /**
     * This constructor is only useful by the database object after performing a lookup.
     * It's meant to represent a token that was previously created. It will not generate
     * a valid authentication token
     *
     * @param authToken     String representation of the authentication token that can be used by the client.
     * @param createdDate   Timestamp of when the token was originally created.
     * @param IP            The IP that the user connected with when the token was generated.
     * @param user          The username associated with the authentication token.
     */
    public AuthToken(String authToken, Date createdDate, String IP, String user) {
        this.authToken = authToken;
        this.createdDate = createdDate;
        this.IP = IP;
        this.user = user;
    }

    private AuthToken(String IP, String User) {
        this.IP = IP;
        this.user = User;
        this.createdDate = new Date();

        //Generate the authToken
        //Generate a random number of 512 bits
        SecureRandom secureRandom = new SecureRandom();
        byte bytes[] = new byte[64];
        secureRandom.nextBytes(bytes);

        //Hash these with SHA512
        //Output will be our token
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
            byte output[] = messageDigest.digest(bytes);

            //Encode in Base64 for client side use
            byte encoded[] = Base64.getEncoder().encode(output);

            this.authToken = new String(encoded);
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        //Store in database
        PostgresConnector.saveAuthToken(this);
    }

    /**
     * Given an authentication token and request body, this function will
     * validate whether or not the users authentication is still good. If
     * the token is expired, or if the IP address has changed, the old token
     * is invalidated and deleted from the database.
     *
     * @param authToken The string provided by the client connection
     * @param request   The HTTP request sent from the client machine
     * @return          Returns true if the authentication token is still valid.
     */
    public static boolean isGoodAuthToken(String authToken, Request request) {
        //Lookup authToken in database
        AuthToken currentAuthToken = PostgresConnector.getAuthToken(authToken);

        //Check that the date is no longer than 1
        if (currentAuthToken != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentAuthToken.createdDate);
            calendar.add(Calendar.HOUR_OF_DAY, LIFESPAN);

            if (calendar.getTime().before(new Date())) {
                //AuthToken expired
                PostgresConnector.deleteAuthToken(authToken);
                return false;
            }

            //Check that the IP hasn't changed
            if (!currentAuthToken.getIP().equals(request.ip())) {
                System.out.println("here!");
                //AuthToken invalid do to another connection
                PostgresConnector.deleteAuthToken(authToken);
                return false;
            }
            //return true if everything is good
            return true;
        }

        //If auth token was not found, then it's not a good token.
        return false;
    }

    /**
     * Creates a new authentication token and stores it in the database after the Login Controller validates the
     * users login information.
     *
     * @param IP    The IP address the users connected to the site with (found in the request body).
     * @param User  The user to generate a new authentication token for.
     * @return      Returns the authentication token in Base64 for the clients use.
     */
    public static String newAuthToken(String IP, String User) {
        //Generate authToken
        AuthToken addedAuthToken = new AuthToken(IP, User);

        return addedAuthToken.getAuthToken();
    }

    /**
     * Get the authentication token in Base64 format.
     *
     * @return  The Base64 representation of the authentication token for use on the client side.
     */
    public String getAuthToken() {
        return authToken;
    }

    /**
     * Get the IP address attached to the authentication token.
     *
     * @return  The original connecting IP associated with the authentication token.
     */
    public String getIP() {
        return IP;
    }

    /**
     * Get the username associated with the authentication token.
     *
     * @return  The username associated with the authentication token.
     */
    public String getUser() {
        return user;
    }

    /**
     * Get the date associated with the authentication token.
     *
     * @return  The timestamp of when the authentication token was generated.
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * Returns the authToken since that's all that's all we really
     * care about outside of this class.
     *
     * @return  The generated authentication token.
     */
    @Override
    public String toString() {
        return authToken;
    }
}
