package Authentication;

import mappings.LoginAttempt;
import spark.Request;

/**
 * Created by brad on 1/24/15.
 */
public class LoginController {
    private static final String AUTHTOKEN = "auth-token";

    public static void logOut(Request request) {
        request.session(true);
        String token = request.session().attribute(AUTHTOKEN);

        //Remove the authtoken and delete it from the database
        if (token != null) {
            AuthToken authToken = PostgresConnector.getAuthToken(token);
            if (authToken != null) {
                PostgresConnector.deleteAuthToken(authToken.getUser());
            }
            request.session().removeAttribute(AUTHTOKEN);
        }
    }

    public static boolean isLoggedIn(Request request) {
        request.session(true);
        String authToken = request.session().attribute(AUTHTOKEN);
        //If there is an AuthToken, and it's good.
        return authToken != null && AuthToken.isGoodAuthToken(authToken, request);
    }

    public static boolean isGoodCredentials(LoginAttempt loginAttempt) {
        //Delete all other authtokens for this user.
        PostgresConnector.deleteAuthToken(loginAttempt.getUsername());
        return PostgresConnector.checkUser(loginAttempt.getUsername(), loginAttempt.getPasswordHash());
    }

    public static void issueToken(Request request, LoginAttempt loginAttempt) {
        request.session().attribute(AUTHTOKEN,
                AuthToken.newAuthToken(request.ip(), loginAttempt.getUsername()));
    }

    public static String getLoggedInUser(Request request) {
        request.session(true);
        if (request.session().attribute(AUTHTOKEN) != null) {
            AuthToken token = PostgresConnector.getAuthToken(request.session().attribute(AUTHTOKEN));
            if (token != null) {
                return token.getUser();
            }
        }
        return "";
    }
}
